import React, { useCallback, useState, useEffect } from 'react';
import { muiBankTheme, MetronomeProvider } from '@ally/metronome-ui'
import './App.css';
import SearchForm from './components/SearchForm';
import ResultsTable from './components/ResultsTable';
import Pagination from './components/Pagination';
import { searchByTitle } from './services/movies';

function App() {
  const [searchResults, setResults] = useState([]);
  const [totalCount, setTotalCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchString, setSearchString] = useState('');
  
  const searchAndSetCallback = useCallback(async (titleStr) => {
    const { total, movies } = await searchByTitle(titleStr, currentPage);
    setSearchString(titleStr);
    setResults(movies);
    setCurrentPage(1);
    setTotalCount(total);
  }, [setResults, setCurrentPage, setTotalCount, currentPage]);

  const resetResults = useCallback(() => {
    setResults([]);
    setCurrentPage(1);
    setTotalCount(0);
  }, [setResults, setCurrentPage, setTotalCount]);

  const setPage = useCallback((page) => setCurrentPage(page), [setCurrentPage]);

  useEffect(() => {
    (async () => {
      const { total, movies } = await searchByTitle(searchString, currentPage);
      setTotalCount(total);
      setResults(movies);
    })()
  }, [setResults, setTotalCount, searchString, currentPage]);

  return (
    <MetronomeProvider theme={muiBankTheme}>
      <div className="App">
        <header className="App-header">
          <SearchForm onSubmit={searchAndSetCallback} clearResults={resetResults} />
          <Pagination onPageClick={setPage} totalCount={totalCount} activePage={currentPage} />
          <ResultsTable movies={searchResults} />
          <Pagination onPageClick={setPage} totalCount={totalCount} activePage={currentPage} />
        </header>
      </div>
    </MetronomeProvider>
  );
}

export default App;
