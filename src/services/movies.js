import axios from 'axios';
import { setupCache } from 'axios-cache-adapter';
import { OMDB_BASE_URL, OMDB_KEY } from '../constants';

const { adapter } = setupCache({
  maxAge: 120 * 60 * 1000,
  exclude: {
    query: false,
  },
});

const client = axios.create({
  adapter,
});

export const searchByTitle = async (titleStr, page = 1) => {
  // TODO - Implement search by title - return in format { total: Number, movies: Array of movie objects}
}

export const fetchById = async (id) => {
  // TODO - implement fetch by id - return the response body from the api request
}
