import React, { useCallback, useState, useEffect } from 'react';
import { Drawer, DrawerContent, DrawerPanel } from '@ally/metronome-ui';
import styled from 'styled-components';
import MovieDetails from '../MovieDetails';
import { fetchById } from '../../services/movies';

const MovieSummary = ({ triggerRef, movie, onClickHandler, toggleHiddenPanel = () => { } }) => (
  <div className="movie-summary-container">
    <div className="movie-summary-image">
      <img alt={movie.Title} src={movie.Poster} />

      <div>
        <a href="/" ref={triggerRef} onClick={(e) => { onClickHandler(e); toggleHiddenPanel(e) }}>View Details</a>
      </div>
    </div>
    <div className="movie-summary-text">
      <div>{movie.Title}</div>
      <div>{movie.Year}</div>


    </div>
  </div>);

const ResultsTable = ({ movies, className = '' }) => {
  const [movieData, setMovieData] = useState({});
  const [activeMovieId, setActiveMovieId] = useState('');
  const [activeMovieData, setActiveMovieData] = useState({});

  const addMovieEntry = useCallback((id, data) => {
    setMovieData({ ...movieData, [id]: data });
  }, [movieData, setMovieData]);

  const getMovieEntry = useCallback((id) => movieData[id], [movieData]);

  const onViewDetailsClick = useCallback((imdbId) => async (e) => {
    e.preventDefault();
    setActiveMovieId(imdbId);
  }, [setActiveMovieId]);

  useEffect(() => {
    (async () => {
      if (!!activeMovieId) {
        // TODO - Implement use Effect
      }
    })()
  }, [activeMovieId, addMovieEntry, setActiveMovieData, getMovieEntry]);


  return (
    <div className={className}>
      <ul>
        {movies.map(movie => (
          <li key={movie.imdbID}>
            <div className="drawer-item-container">
              <Drawer>
                <DrawerContent>
                  <MovieSummary movie={movie} onClickHandler={onViewDetailsClick(movie.imdbID)} />
                </DrawerContent>
                <div className="drawer-panel-container">
                  <DrawerPanel>
                    <MovieDetails movie={activeMovieData} />
                  </DrawerPanel>
                </div>
              </Drawer>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

// TODO - Implement styles
const StyledResultsTable = styled(ResultsTable)``;

export default StyledResultsTable;
