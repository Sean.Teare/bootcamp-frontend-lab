import { Button, TextInput } from '@ally/metronome-ui';
import styled from 'styled-components';
import { useCallback, useState } from 'react';

const SearchForm = ({ onSubmit = () => { }, clearResults = () => { }, className = '' }) => {
  const submitForm = useCallback((e) => {
    e.preventDefault();
    onSubmit(e.target.title.value);
  }, [onSubmit]);
  
  return (
    <div className={className}>
      <form onSubmit={submitForm} className={className}>
        {/* TODO - implement form */}
      </form>
    </div>

  );
}

// TODO - implement styles
const StyledSearchForm = styled(SearchForm)``;

export default StyledSearchForm;
