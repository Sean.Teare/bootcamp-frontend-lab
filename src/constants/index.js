export const OMDB_BASE_URL = 'http://www.omdbapi.com';
// TODO - Insert omdb api key here
export const OMDB_KEY = '';

const constants = {
  OMDB_BASE_URL,
  OMDB_KEY,
};

export default constants;
