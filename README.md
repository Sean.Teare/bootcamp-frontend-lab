# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Bootcamp Instructions

Implement TODOs in following files

- constants/index.js
- components/SearchForm/index.jsx
- components/ResultsTable/index.jsx
- components/Pagination/index.jsx
- components/MovieDetails/index.jsx
- services/movies.js

## Useful references

- http://www.omdbapi.com/ - Specifically Usage & Parameters Section
- https://axios-http.com/docs/api_intro
- https://styled-components.com/docs/basics#getting-started
